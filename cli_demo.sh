#!/bin/sh -x

#SSH_COMMAND='ssh -p 3000 rockstor@localhost'
#SSH_COMMAND='ssh rockstor@192.168.1.25'
SSH_COMMAND='ssh rockstor@192.168.2.12'


echo "List of disks"
$SSH_COMMAND "disks list"
echo "Running setup"
$SSH_COMMAND "setup disks"
echo "done"

read foo
$SSH_COMMAND "pools list"
echo "Adding a pool"
$SSH_COMMAND "pools add -dsdb,sdc -rraid0 -npool0"
echo "Done"

read foo
echo "--------SHARE----------"
$SSH_COMMAND "shares list"
echo "Adding a share"
$SSH_COMMAND "shares add -ppool0 -nshare1 -s12345"
echo "Done"

read foo
echo "--------NFS------------"
echo "Adding a nfs export"
$SSH_COMMAND "shares share share1 nfs add_export -cexample.com -mro -sasync"
read foo
echo "Disabling it"
$SSH_COMMAND "shares share share1 nfs disable_export 1"
read foo
echo "Enabling it"
$SSH_COMMAND "shares share share1 nfs enable_export 1"
read foo
echo "deleting it"
$SSH_COMMAND "shares share share1 nfs delete_export 1"

read foo
echo "Adding bulk nfs exports"
$SSH_COMMAND "shares share share1 nfs add_export -cexample2.com -mrw -ssync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample3.com -mro -sasync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample4.com -mro -sasync"
$SSH_COMMAND "shares share share1 nfs add_export -cexampl5.com -mro -ssync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample6.com -mro -sasync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample7.com -mrw -sasync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample8.com -mro -sasync"
$SSH_COMMAND "shares share share1 nfs add_export -cexample9.com -mrw -ssync"
echo "Done"

read foo
echo "----------SMB--------------"
echo "Adding samba"
$SSH_COMMAND "shares share share1 smb add -byes -gno -rno"
read foo
echo "Disabling"
$SSH_COMMAND "shares share share1 smb disable"
read foo
echo "Enabling back"
$SSH_COMMAND "shares share share1 smb enable"
read foo
echo "Deleting samba"
$SSH_COMMAND "shares share share1 smb delete"
echo "Done"

read foo
echo "---------BULK SHARES--------"
$SSH_COMMAND "shares add -ppool0 -nshare2 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare3 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare4 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare5 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare6 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare7 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare8 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare9 -s12345"
$SSH_COMMAND "shares add -ppool0 -nshare10 -s12345"
$SSH_COMMAND "shares list"
echo "Done"

read foo
echo "Adding snapshot"
$SSH_COMMAND "shares share share1 add_snapshot snap1"
read foo
echo "Deleting it"
$SSH_COMMAND "shares share share1 delete_snapshot snap1"
echo "Done"

read foo
echo "Adding bulk snapshots"
$SSH_COMMAND "shares share share2 add_snapshot snap1"
$SSH_COMMAND "shares share share2 add_snapshot snap2"
$SSH_COMMAND "shares share share2 add_snapshot snap3"
$SSH_COMMAND "shares share share2 add_snapshot snap4"
$SSH_COMMAND "shares share share2 add_snapshot snap5"
$SSH_COMMAND "shares share share2 add_snapshot snap6"
$SSH_COMMAND "shares share share2 add_snapshot snap7"
$SSH_COMMAND "shares share share2 add_snapshot snap8"
$SSH_COMMAND "shares share share2 add_snapshot snap9"
$SSH_COMMAND "shares share share2 add_snapshot snap10"
echo "Done"
