#notes and script to get latest btrfs code in ubuntu

#for the latest kernel source tree, there's kernel.org but it is slightly
#behind chris mason's unstable tree.
# git clone git://git.kernel.org/pub/scm/linux/kernel/git/mason/linux-btrfs.git

#for more bleeding edge nightly tree use josef bacik's
# git clone git://git.kernel.org/pub/scm/linux/kernel/git/josef/btrfs-next.git

#there is no corresponding nightly tree for btrfs-progs, perhaps we should
#start doing that outselves.

#kernel compilation in one of the above linux source trees
# cd linux-btrfs
# cp -vi /boot/config-`uname -r` .config
# for make menuconfig
# sudo apt-get install libncurses5 libncurses5-dev
# sudo apt-get install kernel-package
# make menuconfig
# for clean build
# make-kpkg clean
# export CONCURRENCY_LEVEL=3 (or whatever is appropriate)
# fakeroot make-kpkg --initrd --append-to-version=-some-string-here kernel-image kernel-headers
#
