#Copyright (c) 2012 MemeGarden, Inc. <http://rockstor.com>
#This file is part of RockStor.

#RockStor is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published
#by the Free Software Foundation; either version 3 of the License,
#or (at your option) any later version.

#RockStor is distributed in the hope that it will be useful, but
#WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
#!/bin/sh

#Copyright (c) 2012 MemeGarden, Inc. <http://rockstor.com>
#This file is part of RockStor.

#RockStor is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published
#by the Free Software Foundation; either version 3 of the License,
#or (at your option) any later version.

#RockStor is distributed in the hope that it will be useful, but
#WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

install_deps () {
    apt-get install -y git curl python-pip
    apt-get purge btrfs-tools
    dpkg -i bin/debs/btrfs-progs_1.0-1_amd64.deb
    apt-get install -y python-setuptools
    #uninstall apache2 as it seems to be installed in dev vanilla environments
    apt-get remove apache2
    # for nfs server
    apt-get install nfs-kernel-server
    apt-get install samba
    apt-get install -y build-essential gawk alien fakeroot linux-headers-$(uname -r)
    apt-get install -y zlib1g-dev uuid-dev libblkid-dev libselinux-dev parted lsscsi
    pip install --upgrade Django
    pip install gunicorn
    easy_install-2.7 -U distribute
    easy_install-2.7 URLObject
    easy_install-2.7 djangorestframework
    easy_install-2.7 pytz
    easy_install-2.7 django-pipeline
    apt-get install -y python-flup nginx
    useradd nginx
    passwd -d nginx

    useradd -m rockstor -s /usr/share/RockStor/cli/rock_cli.py
    usermod -s /usr/share/RockStor/cli/rock_cli.py -p '$6$q8K.5Ilg$qvXGdH3RAFdtp.mTPNkAbrKGjLF3ONwDUtT1BX3zsCCUhTD6zJYQvuGrFwKgw0Sftl8fmHvquQFeVnAciUG5g/' rockstor

    # for streaming support
    pip install socketIO-client
    apt-get install python-dev
    apt-get install libevent-dev
    pushd /tmp
    git clone https://github.com/abourget/gevent-socketio.git
    pushd gevent-socketio.git
    python setup.py install
    popd
    popd
}

if [ $# -eq 0 ]; then
    install_deps
fi

rm -rf js
python manage.py collectstatic --noinput -i admin

if [ -d /usr/share/RockStor ]; then
  rm -rf /usr/share/RockStor
fi
mkdir /usr/share/RockStor
cp -rf fs /usr/share/RockStor/
cp -rf system /usr/share/RockStor/
cp -rf conf /usr/share/RockStor/
cp -rf templates /usr/share/RockStor/
rm -rf /usr/share/RockStor/templates/storageadmin/js/js
cp -rf js /usr/share/RockStor/templates/storageadmin/
cp -rf RockStor /usr/share/RockStor/
cp -rf storageadmin /usr/share/RockStor/
cp manage.py /usr/share/RockStor/
cp -rf cli /usr/share/RockStor/
chmod a+x /usr/share/RockStor/cli/rock_cli.py
cp -rf certs /usr/share/RockStor/

sudo rm -rf /mnt2
sudo mkdir /mnt2

mv /etc/nginx/nginx.conf /etc/nginx/nginx-backup.conf
cp conf/nginx.conf /etc/nginx/

for pid in `ps -ef | grep gunicorn | awk '{print $2}'`; do kill -9 $pid; done

export PYTHONPATH='/usr/share/RockStor'
#python /usr/share/RockStor/manage.py runfcgi host=127.0.0.1 port=8080 --pythonpath="/usr/share/RockStor" --settings=RockStor.settings
gunicorn --worker-class socketio.sgunicorn.GeventSocketIOWorker RockStor.wsgi:application &
/etc/init.d/nginx restart