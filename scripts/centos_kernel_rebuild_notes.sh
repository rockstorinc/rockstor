#!/bin/sh

su -c 'yum install kernel-devel'

mkdir -p ~/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
echo '%_topdir %(echo $HOME)/rpmbuild' > ~/.rpmmacros

su -c 'yum install rpm-build redhat-rpm-config asciidoc hmaccalc'
su -c 'yum install binutils-devel elfutils-libelf-devel newt-devel zlib-devel'

#rpm -i http://vault.centos.org/6.3/updates/Source/SPackages/kernel-2.6.32-279.5.1.el6.src.rpm 2>&1 | grep -v mock

su -c 'yum install xmlto python-devel perl-ExtUtils-Embed'
cd ~/rpmbuild/SPECS
su -c 'rngd -r /dev/urandom'
rpmbuild -bp --target=$(uname -m) kernel.spec

#building a kernel module
#make necessary changes inside btrfs directory
#have two copies of top level linux-... directory
#make changes in one
#create patch with diff -uNr linux-olddir.. linux-newdir.. > btrfs_patch_file
#apply patch by cd linux-odldir; patch -p1 ../btrfs_path_file
#now ready to compile and build btrfs.ko

#followed: wiki.centos.org/HowTos/BuildingKernelModules
#remember to modprobe -f btrfs
