"""
Copyright (c) 2012 MemeGarden, Inc. <http://rockstor.com>
This file is part of RockStor.

RockStor is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

RockStor is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""

#!/bin/sh

yum -y install python-pip
/usr/bin/pip-python install Django
/usr/bin/pip-python install djangorestframework

if [ -d /usr/local/storaze ]; then
  rm -rf /usr/local/storaze
fi
mkdir /usr/local/storaze
cp -rf fs /usr/local/storaze/
cp -rf system /usr/local/storaze/
cp -rf templates /usr/local/storaze/
cp -rf zappv1 /usr/local/storaze/

yum -y install python-flup nginx


mv /etc/nginx/nginx.conf /etc/nginx/nginx-backup.conf
cp conf/nginx.conf /etc/nginx/

for pid in `ps -ef | grep runfcgi | awk '{print $2}'`; do kill -9 $pid; done

export PYTHONPATH='/usr/local/storaze/'
python /usr/local/storaze/zappv1/manage.py runfcgi host=127.0.0.1 port=8080 --pythonpath="/usr/local/storaze/zappv1" --settings=settings

/usr/sbin/nginx -s stop
/usr/sbin/nginx
