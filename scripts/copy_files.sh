python manage.py collectstatic --noinput -i admin

cp -rf fs /usr/share/RockStor/
cp -rf storageadmin /usr/share/RockStor/
cp -rf system /usr/share/RockStor/
cp -rf templates /usr/share/RockStor/
rm -rf /usr/share/RockStor/templates/storageadmin/js/js
cp -rf js /usr/share/RockStor/templates/storageadmin/
cp -f RockStor/urls.py /usr/share/RockStor/RockStor/

for pid in `ps -ef | grep runfcgi | awk '{print $2}'`; do kill -9 $pid; done

export PYTHONPATH='/usr/share/RockStor'
#python /usr/share/RockStor/manage.py runfcgi host=127.0.0.1 port=8080 --pythonpath="/usr/share/RockStor" --settings=RockStor.settings

#/etc/init.d/nginx restart

