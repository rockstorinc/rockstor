#!/bin/sh

openssl req -new > rockstor.cert.csr
openssl rsa -in private_key.pem -out rockstor.cert.key
openssl rsa -in privkey.pem -out rockstor.cert.key
openssl x509 -in rockstor.cert.csr -out rockstor.cert.cert -req -signkey rockstor.cert.key -days 1825

