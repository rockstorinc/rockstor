#!/bin/sh

# This script more or less works. Here are the prerequisites
#1. In your host OS install virtualbox or your favorite vm platform
#In Ubuntu, it is as simple as sudo apt-get install -y virtualbox
#
#2. Download ubuntu 12.04 iso and create a vm in virtualbox with it.
#
#3. In addition to the root disk, create a few small disks in settings->storage of virtualbox. I recommend 4 or 8.
#
#4. Once your vm is fully operational, git clone this repo, cd and execute this script.

apt-get install -y git curl
apt-get install -y python-setuptools
apt-get install -y python-django
easy_install-2.7 -U distribute
easy_install-2.7 djangorestframework

PYPATH=`pwd`
export PYTHONPATH=$PYPATH
python manage.py runserver