"""
Copyright (c) 2012 MemeGarden, Inc. <http://rockstor.com>
This file is part of RockStor.

RockStor is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published
by the Free Software Foundation; either version 3 of the License,
or (at your option) any later version.

RockStor is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

"""


#!/bin/bash

if [ -d debian ]; then
    rm -rf debian
fi


mkdir debian
mkdir -p debian/usr/share/
cp -r /usr/local/RockStor debian/usr/share/

mkdir debian/DEBIAN
cp control debian/DEBIAN/
cp config debian/DEBIAN/
cp conffiles debian/DEBIAN/
cp postinst debian/DEBIAN/
cp postrm debian/DEBIAN/
cp preinst debian/DEBIAN/
cp prerm debian/DEBIAN/


mkdir -p debian/etc/init.d
cp rockstor debian/etc/init.d/

mkdir -p debian/etc/samba
cp /usr/local/RockStor/conf/smb.conf debian/etc/samba/
mkdir -p debian/etc/nginx
cp /usr/local/RockStor/conf/nginx.conf debian/etc/nginx/

mkdir -p debian/usr/share/doc/rockstor
cp changelog changelog.Debian copyright debian/usr/share/doc/rockstor/
gzip --best ./debian/usr/share/doc/rockstor/changelog
gzip --best ./debian/usr/share/doc/rockstor/changelog.Debian

find debian -type d | xargs chmod 755

fakeroot dpkg-deb --build debian
mv debian.deb RockStor-1.0_all.deb
lintian RockStor-1.0_all.deb
