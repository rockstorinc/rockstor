#!/usr/bin/env python

from setuptools import setup

VERSION = '1.0.1'

setup(
  name='rockstor',
  version=VERSION,
  description='Smart Powerful Storage Solution',
  author='Suman Chakravartula',
  author_email='schakrava@gmail.com',

  packages=['storageadmin', 'smart_manager',],
  package_dir={'': 'src/rockstor'},
  entry_points={
        'console_scripts': [
            'sm = smart_manager.smd:main',
            'wc = websocket_client.cpu_info:main',
            'rcli = cli.rock_cli:main',
            'prep_db = prep_db:main',
            ],
        },

  dependency_links = ['http://rockstor.com/downloads/gevent-socketio-0.3.6.tgz'],

  install_requires=[
    'django == 1.4.3',
    'distribute >= 0.6.35',
    'URLObject == 2.1.1',
    'djangorestframework == 2.1.15',
    'pytz',
    'django-pipeline == 1.2.23',
    'socketIO-client == 0.3',
    'gevent-socketio == 0.3.6',
    'requests == 1.1.0',
    'pyzmq == 13.0.0',
  ]
)
