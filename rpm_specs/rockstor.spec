Name: rockstor
Version: 1.3
Release: 0
Summary: RockStor -- Smart Powerful Storage
Group: RockStor

%define source %{name}-%{version}
%define rock_user rocky
%define rock_group rocky
%define rock_shell /opt/rockstor/src/rockstor/cli/rock_cli.py
%define rock_pw $6$q8K.5Ilg$qvXGdH3RAFdtp.mTPNkAbrKGjLF3ONwDUtT1BX3zsCCUhTD6zJYQvuGrFwKgw0Sftl8fmHvquQFeVnAciUG5g/

Vendor: RockStor, Inc.
Packager: RockStor, Inc. <support@rockstor.com>
License: GPL
AutoReqProv: no
Source: %{source}.tgz
Prefix: /opt
BuildRoot: /tmp/%{name}
Requires: nginx
Requires: python27
Requires: kernel-ml

%description
%{summary}

%prep
%setup -n %{source}

%build
rm -rf %{buildroot}
mkdir %{buildroot} %{buildroot}/opt
cp -r $RPM_BUILD_DIR/%{source} %{buildroot}/opt/%{name}
cd %{buildroot}/opt/%{name}
python2.7 install.py bootstrap
python2.7 install.py buildout:extensions=

python2.7 -m compileall -q -f -d /opt/%{name}/eggs eggs \
   > /dev/null 2>&1 || true

rm -rf release-distributions

for egglink in develop-eggs/*.egg-link
do
    sed -i "s;%{buildroot};;" ${egglink}
done

cd /tmp

%clean
rm -rf %{buildroot}
rm -rf $RPM_BUILD_DIR/%{source}

%files
%defattr(-, root, root)
/opt/%{name}

%pre
getent group %{rock_group} >/dev/null || groupadd -r %{rock_group}
getent passwd %{rock_user} >/dev/null || \
    useradd -r -g %{rock_group} -s %{rock_shell} \
    -p %{rock_pw} -c "Rocky Balboa" %{rock_user}
exit 0

%post
mkdir -p /opt/rockstor/var/log
mkdir -p /opt/rockstor/var/run
exit 0

%postun
getent passwd %{rock_user} >/dev/null && userdel %{rock_user}
getent group %{rock_group} >/dev/null && groupdel %{rock_group}
exit 0